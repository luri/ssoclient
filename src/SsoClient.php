<?php
/**
 *
 * (c) Luri <offluri@gmail.com>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * Sso Client is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */

namespace Luri\Sso;

use Http\Client\HttpClient;
use Http\Message\MessageFactory;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\MessageFactoryDiscovery;
use \Firebase\JWT\JWT;
use Luri\Sso\Exception\HttpError;
use Luri\Sso\SessionSimple;

if (!defined('SSO_TOKEN_RENEW')) define('SSO_TOKEN_RENEW', "+20 min");
if (!defined('SSO_TOKEN_EXPIRED')) define('SSO_TOKEN_EXPIRED', "+60 min");
/**
 * @todo Add Logguer interface
 */
class SsoClient implements \ArrayAccess, \IteratorAggregate {
	 /**
     * @var HttpClient
     */
    protected $httpClient;
	/**
     * @var MessageFactory
     */
    protected $messageFactory;


	/**
	 * URL du server
	 *
	 * For example : https://sso.domain.fr/
	 *
	 * @var string
	 */
	protected $urlServer;
	/**
	 * Secret share with Server
	 *
	 * (use for generate session key)
	 * @var string
	 */
	protected $secret;
	/**
	 * Apps identify for server
	 * @var string
	 */
	protected $idenApps;
	/**
	 * True if user must be logged to access
	 * @var bool
	 */
	protected $mustBeLoggued;

	/**
	 * Relative url to login page in sso server
	 * @var string
	 */
	static $urlLogin = '/login.php';
	/**
	 * Relative url for get user info in sso server
	 * @var string
	 */
	static $urlAPICommand = '/command.php';

	/**
	 * user is logged or not
	 * indique si l'utiliseur est identifié ou pas
	 * @var boolean
	 */
	protected $isLogged = false;
	/**
	 * user Info
	 *
	 * Min response :
	 *   ['anonymous'] --> True (not loggedà / false (logged)
	 *   ['pseudo']
	 *   ['level'] --> 0 user standard, >0:  admin level
	 *
	 * @var array
	 */
	protected $userInfo = "";

	/**
	 * Simple Session handler for implement standard security advice
	 *
	 * Access Session variable with ArrayAccess ['xxx']
	 *
	 * Use it instead of $this->session.
	 *
	 * @var \Luri\Sso\SessionSimple
	 */
	public $session;

	/**
	 * Start an SSO session
	 * we use PHP session and redirect function
	 *
	 * @param string $urlServer URL of identity server
	 * @param string $secret share secret with server
	 * @param string $idenApps Identity of this apps
	 * @param bool $mustBeLoggued Tel if user must be loggued or not (false by default)
	 * @param HttpClient|null $httpClient Client to do HTTP requests, if not set, auto discovery will be used to find a HTTP client.
	 * @param MessageFactory|null $messageFactory to create PSR-7 requests.
	 */
	public function __construct($urlServer, $secret, $idenApps, $mustBeLoggued=false, HttpClient $httpClient = null, MessageFactory $messageFactory = null) {
		$this->urlServer = $urlServer;
		$this->secret = $secret;
		$this->idenApps = $idenApps;
		$this->mustBeLoggued = (bool) $mustBeLoggued;

		//Start Session
		$this->session = new SessionSimple();

		//Store or discovery httpClient and Message
		$this->httpClient = $httpClient ?: HttpClientDiscovery::find();
		$this->messageFactory = $messageFactory ?: MessageFactoryDiscovery::find();

		//Session Flag ?
		if (isset($this->session['deletelocaltoken'])) {
			//Delete token only here
			$this->deleteToken(false);
			unset($this->session['deletelocaltoken']);
		}

		if (isset($this->session['deletetokenonserver'])) {
			//Delete token here and in server
			$this->deleteToken();
			unset($this->session['deletetokenonserver']);
		}

		if (isset($this->session['launcherror400'])) {
			unset($this->session['launcherror400']);
			//Destruction du jeton
			$this->deleteToken();

			//Et de la session
			$this->session->destroySession();

			//Erreur HTTP
			header("HTTP/1.0 400 Bad Request");
			echo "Erreur inconnue";

			if(!defined('PHPUNIT_TESTING')) {
				exit();
			} else {
				//For test
				return;
			}
		}

		//If necessary, générate a new token
		$valid = $this->verifyToken();

		//Here, we have a token for talk to server, ask identity of client to server
		if ($valid) {
			$this->loadIdentify();
		}
	}

	public function __destruct() {
		sodium_memzero($this->secret);
		sodium_memzero($this->idenApps);
	}

	/**
	 * Verify if client have a token and if is valid
	 *
	 * @return boolean
	 */
	protected function verifyToken() {
		if (empty($this->session['token'])) {
			$this->generateAndSendToken();
			return false;
		}
		if (empty($this->session['tokenDate']) OR empty ($this->session['tokenCreationDate'])) {
			$this->deleteToken();
			$this->generateAndSendToken();
			return false;
		}

		//Is token is expired ?
		$daterenew = new \DateTime($this->session['tokenDate']);
		$daterenew->modify(SSO_TOKEN_RENEW);

		$dateexpired = new \DateTime($this->session['tokenCreationDate']);
		$dateexpired->modify(SSO_TOKEN_EXPIRED);

		$now = new \DateTime();

		if ($now > $dateexpired) {
			//Token expired
			$this->deleteToken();
			$this->generateAndSendToken();
			return false;
		}

		if ($now > $daterenew) {
			//Token is a little too old, Renew Token
			$this->renewToken();
			return false;
		}

		return true;

	}

	protected function renewToken() {
		//Update tokenDate but no change to tokenCreationDate
		$this->session['tokenDate'] = date(DATE_ATOM);

		//Save page where user is (for return)
		$this->session['originPage'] = $_SERVER['REQUEST_URI'];

		//Param
		$param_app = htmlentities($this->idenApps);

		//URL and Query
		$url = self::urlConcat($this->urlServer, self::$urlLogin);

		//USE POST variable, so we must generate a form html and send them by javascript
		echo <<<VAR
<html>
	<body>
		<form id="ed" action="$url" method="post">
			<input type="hidden" name="command" value="renew" />
			<input type="hidden" name="app" value="$param_app" />
		</form>
		<p>Veuillez patientez, validation de l'acc&egrave;s en cour...</p>
		<script type="text/javascript">
			document.getElementById('ed').submit();
		</script>
	</body>
</html>
VAR;

		if(!defined('PHPUNIT_TESTING')) {
			exit();
		}
	}

	/**
	 * Delete token and inform server
	 *
	 * @param bool $deleteInServer False if Token is invalid over SSO server
	 */
	protected function deleteToken($deleteInServer = true) {
		//Say to server token is expired
		if($deleteInServer) {
			$this->sendCommandToServer('DeleteToken');
		}

		//Delete token here
		$this->session['token']="";
		unset($this->session['token']);
		unset($this->session['tokenDate']);

		//For security, Régen session
		$this->session->regenerateSession();
	}

	/**
	 * Generate a new token and send it to server by client
	 *
	 * The public token is stocked into a session variable
	 *
	 * @param string $msg User message sended to login page
	 */
	protected function generateAndSendToken($msg='') {
		//generate token and date
		$this->session['token'] = base64_encode(openssl_random_pseudo_bytes(60));
		$this->session['tokenDate'] = date(DATE_ATOM);
		$this->session['tokenCreationDate'] = date(DATE_ATOM);

		//Save page where user is (for return)
		$this->session['originPage'] = $_SERVER['REQUEST_URI'];

		//Say to client to send this to serve(r
		// Cette requète passe par le client qui est potentiellement un pirate, on ne veut donc pas lui donner de "signature"
		// contenant le secret qu'il pourrait éventuellement attaquer
		// Toutes les requètes entre le client SSO et le serveur d'auth seront protégés en utilisant un JWT. Un attaquant ne peut donc exploiter le token et le nom de l'appli.

		//Param
		$param_mustbeloggued = ($this->mustBeLoggued) ? 'true' : 'false';
		$param_token = $this->session['token'];
		$param_app = htmlentities($this->idenApps);
		$msg = htmlentities($msg);

		//URL and Query
		$url = self::urlConcat($this->urlServer, self::$urlLogin);

		//USE POST variable, so we must generate a form html and send them by javascript
		echo <<<VAR
<html>
	<body>
		<form id="ed" action="$url" method="post">
			<input type="hidden" name="command" value="attach" />
			<input type="hidden" name="mustbelogged" value="$param_mustbeloggued" />
			<input type="hidden" name="token" value="$param_token" />
			<input type="hidden" name="app" value="$param_app" />
			<input type="hidden" name="msg" value="$msg" />
		</form>
		<p>Veuillez patientez, validation de l'acc&egrave;s en cour...</p>
		<script type="text/javascript">
			document.getElementById('ed').submit();
		</script>
	</body>
</html>
VAR;

		if(!defined('PHPUNIT_TESTING')) {
			exit();
		}
	}

	/**
	 * Manage Return after a attach or renew command to SSO server
	 *
	 * This function must be called in a return.php page or similary
	 *
	 * This function not include exit() (because unit test). You must add an exit() function after call this.
	 *
	 * @param int $code return code of sso server after a attach or renew command
	 */
	public static function ssoReturn($code) {
		if (!is_numeric($code) OR $code < 0) {
			//C'est bien bizarre...
			$code = 0;
		}
		$code = (int) $code;

		//Start Session
		$session = new SessionSimple();

		if (empty($session['originPage'])) {
			//Mmmm.... comment faire une redirection si on ne sait pas où aller?
			echo 'Erreur : Je ne sais pas d\'où vous venez désolé.';

		} else {
			//On traite le code de retour. S'il y a une erreur, on positionne un flag dans la session
			switch ($code) {
				case 200: //OK, commande exécuté
					break;

				case 204: // 204 : visiteur inconnu
				case 205: // 205 : Jeton déjà utilisé
					$session['deletelocaltoken'] = true;
					break;

				case 400: // 400 : erreur dans les paramètres
				case 500: // 500 : erreur interne lors de l’exécution de la commande
					$session['deletetokenonserver'] = true;
					break;

				case 404: // 404 : commande inconnue
				default :
					//D'une manière ou d'une autre, le circuit n'a pas été respecté
					$session['launcherror400'] = true;
			}

			//Redirection vers la page d'origine
			header('Location: ' . $session['originPage'] , true, 307);
			echo '307 : Temporary Redirect';
			echo 'Redirect to origin page';

			//delete origin Page
			$session['originPage'] = '';
			unset($session['originPage']);
		}
	}






	/**
	 * Send A Command to AuthServer and return response
	 *
	 * If token not exist (return code 204), we delete and regenerate actual token
	 *
	 * @param string $command
	 * @param array $param
	 * @return array JWT decoded into a array
	 * @throws Exception\InvalidArgumentException
	 * @throws Exception\HttpError if http code is different than 200
	 */
	public function sendCommandToServer($command, array $param = []) {
		//verify
		$command = strip_tags($command);
		if (!is_array($param)) {
			throw new Exception\InvalidArgumentException;
		}

		// Generate JWT
		$token = [
			'command' => $command,
			'param' => $param
		];
		$jwt = JWT::encode($token, $this->secret, 'HS512');

		//URL
		$url = self::urlConcat($this->urlServer, self::$urlAPICommand);
		//Add query param
		$url .= '?' . http_build_query([
			'app' => $this->idenApps
		]);

		//create and send request
		$request = $this->messageFactory->createRequest('POST', $url);
		$request->getBody()->write('jwt=' . $jwt);
		$response = $this->httpClient->sendRequest($request);

		//Verify response
		if ($response->getStatusCode() != 200) {
			//probablement une erreur
			throw new HttpError('Error ' . $response->getStatusCode(), $response->getStatusCode());
		}

		//reponse ok
		$jwtDecoded = (array) JWT::decode($response->getBody(), $this->secret, array('HS512'));
		if (isset($jwtDecoded['result'])) {
			$jwtDecoded['result'] = (Array) $jwtDecoded['result'];
		}

		//Token security
		if ($jwtDecoded['code']==204) {
			//Token no exist, regénérate
			$this->deleteToken(false);
			$this->generateAndSendToken();
			return false; //for test
		}

		return $jwtDecoded;
	}

	/**
	 * Load client identify on server
	 * also, with load other information of client (mail...)
	 */
	protected function loadIdentify() {

		//Min Response :
		// ['anonymous'] --> True (not loggedà / false (logged)
		// ['pseudo']
		// ['level'] --> 0 user standard, >0:  admin level
		$this->userInfo = $this->sendCommandToServer('GetUserInfo', ['token' => $this->session['token']]);

		if ($this->userInfo=== false) {
			//this can happen only in phpunit test mode
			return false;
		}

		//Verify response
		if ($this->userInfo['code'] != 200) {
			//An Error is occured
			$this->deleteToken();
			$this->generateAndSendToken();
			return false; //for test
		}

		$this->userInfo = $this->userInfo['result'];


		//Is user is logged ?
		$this->isLogged = ! $this->userInfo['anonymous'];

		//Verify if user is loggued for some page
		if ($this->mustBeLoggued AND !$this->isLogged) {
			//Force to be Loggued
			$this->deleteToken();
			$this->generateAndSendToken('Sorry, you must be Loggued to access this page');
		}

		//Protect default value
		$this->userInfo['pseudo'] = strip_tags($this->userInfo['pseudo']);
		$this->userInfo['level'] = (int) $this->userInfo['level'];
		if ($this->userInfo['level']<0) $this->userInfo['level'] = -1;



	}



	/**
	 * Is the client is logged into server identity ?
	 *
	 * @return boolean
	 */
	public function isLogged() {
		return $this->isLogged;
	}

	/**
	 * Get client pseudo
	 *
	 * @return string
	 */
	public function getPseudo() {
		return $this->userInfo['pseudo'];
	}

	/**
	 * Get admin level
	 * (0 = standard user)
	 * (-1 = Not Logged user)
	 *
	 * @return int
	 */
	public function getAdminLevel() {
		return $this->userInfo['level'];
	}

	/*****************************
	 *   Array Access Interface  *
	 *****************************/
	public function offsetExists($offset) {
		return array_key_exists($offset, $this->userInfo);
	}


	public function offsetGet ($offset) {
		return $this->userInfo[$offset];
	}
	public function offsetSet ($offset, $value) {
		throw new \BadFunctionCallException('SsoClient["key"] = "toto"; is not permited');
	}
	public function offsetUnset($offset) {
		throw new \BadFunctionCallException('Unset(SsoClient["key"]) is not permited');
	}

	/******************************************************
	 *   Return Iterator for use this class with foreach  *
	 ******************************************************/
	public function getIterator() {
		return new \ArrayIterator($this->userInfo);
	}

	/**********************
	 *   Helper function  *
	 **********************/
	/**
	 * Contact 2 part of 1 url with avoid no or two slash
	 *
	 * @param string $url1
	 * @param string $url2
	 */
	public static function urlConcat($url1, $url2) {
		if (substr($url1, -1)=='/' AND substr($url2, 0, 1)=='/') {
			//2 slash
			$url1 .= substr($url2, 1);
		} elseif (substr($url, -1)!='/' AND substr($url2, 0, 1)!='/') {
			//no slash
			$url1 .= '/' . $url2;
		} else {
			//normal
			$url1 .= $url2;
		}

		return $url1;
	}
}

?>