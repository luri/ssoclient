<?php
/**
 *                      ACCOUNT AUTHENTIFICATION
 *******************************************************************************
 * Simple Session Handler for add a renew mecanism, see https://www.php.net/manual/en/session.security.php
 *
 * (c) Luri <luri@e.email>
 *
 *******************************************************************************
 *                                  LICENCE
 *******************************************************************************
 * sso seerver is distributed with term of CECILL-C licence.
 * Please view Licence_CeCILL-C_V1-en.txt or Licence_CeCILL-C_V1-fr.txt that was
 * distributed with this source code.
 *
 * CECILL-C is a free software license recognised by Open Source Initiative (OSI).
 * This licence is more protective than an L-GPL licence because is protected by
 * French law. (French law not recognise software patent)
 *******************************************************************************
 */
namespace Luri\Sso;


class SessionSimple implements \ArrayAccess {
	/**
	 * Regénération de l'id de session toutes les X secondes
	 */
	const SESSION_REGENERATE = 600;

	/**
	 * Durée de validité d'une session expiré (en secondes)
	 */
	const SESSION_EXPIRED_MAX_TIME = 60;

	/**
	 * Logger
	 * @var \Psr\Log\LoggerInterface
	 */
	protected $logger = null;

	/**
	 * Start a session and verify her validity
	 *
	 * This USE this session var :
	 * $_SESSION['expired']
	 * $_SESSION['dateregenerate']
	 *
	 *
	 * @param int $minAccessLevel
	 * @param \Psr\Log\LoggerInterface $log, if you want log
	 * @throws Exception code 2020 if use an expired session
	 */
	public function __construct(Psr\Log\LoggerInterface $log = null) {
		//Registered Logger
		if ($log instanceof \Psr\Log\LoggerInterface) {
			$this->logger = $log;
		} else {
			$this->logger = new \Psr\Log\NullLogger();
		}

		//Session start
		session_start();

		//Save Generate Time
		if (empty($_SESSION['dateregenerate'])) {
			$_SESSION['dateregenerate'] = time();
		}


		//Delete OLD SESSION
		if (!empty($_SESSION['expired']) AND ($_SESSION['expired'] + self::SESSION_EXPIRED_MAX_TIME) <= time()) {
			//after regenerate an id, some times is let before destroy because concurence access
			//Now, we delete this old session
			$this->logger->alert('Invalid session use. Expired: ' . date('d/m/Y H:i:s', $_SESSION['expired']));
			$this->destroySession();
			throw new \Exception('Session expired', 2020);
		}

		//Periodically regenerate session id for security
		if (($_SESSION['dateregenerate'] + self::SESSION_REGENERATE) <= time()) {
			$this->logger->debug('Regenerate session. date: ' . date('d/m/Y H:i:s', $_SESSION['dateregenerate']));
			$this->regenerateSession();
		}
	}

	/**
	 * Destroy Session and data associated
	 */
	public function destroySession() {
		//delete session variable
		$_SESSION = array();

		//Delete cookie
		if (ini_get("session.use_cookies") ) {
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]
			);
		}
		//Destroy session
		if (session_status() == PHP_SESSION_ACTIVE) {
			session_destroy();

		}
	}

	/**
	 * Regenerate the php session ID.
	 */
	public function regenerateSession() {
		//Invalidate existing session
		$_SESSION['expired'] = time();
		//regen
		session_regenerate_id();
		//revalidate session
		unset($_SESSION['expired']);
		//Save the date
		$_SESSION['dateregenerate'] = time();
	}

	/*****************************
	 *   Array Access Interface  *
	 *****************************/
	public function offsetExists($offset) {
		return array_key_exists($offset, $_SESSION);
	}
	public function offsetGet ($offset) {
		return $_SESSION[$offset];
	}
	public function offsetSet ($offset, $value) {
		$_SESSION[$offset] = $value;
	}
	public function offsetUnset($offset) {
		unset($_SESSION[$offset]);
	}
}
?>